package com.example.todo_app_bba

import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback
import io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService

class Application : FlutterApplication(), PluginRegistrantCallback {
  override fun onCreate() {
    super.onCreate()
    FlutterFirebaseMessagingService.setPluginRegistrant(this)
  }

  override fun registerWith(registry: PluginRegistry) {
    FirebaseCloudMessagingPluginRegistrant.registerWith(registry)
  }
}

object FirebaseCloudMessagingPluginRegistrant {
  fun registerWith(registry: PluginRegistry) {
    if (alreadyRegisteredWith(registry)) {
      return
    }
    FirebaseMessagingPlugin.registerWith(registry.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin"))
  }

  private fun alreadyRegisteredWith(registry: PluginRegistry): Boolean {
    val key: String? = FirebaseCloudMessagingPluginRegistrant::class.java.getCanonicalName()
    if (registry.hasPlugin(key)) {
      return true
    }
    registry.registrarFor(key)
    return false
  }
}