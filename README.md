# Todo Apps
Aplikasi Mobile *(Flutter v1.22.1)* untuk membuat task list dan sharing task dengan user lain.

# Instalasi
Berikut merupakan cara instalasi Todo Apps:
1. Bersihkan Flutter Build Cache dengan cara menjalankan perintah berikut pada terminal:
    > flutter clean

2. Install depedency yang dibutuhkan dengan cara menjalankan perintah berikut pada terminal:
    > flutter pub get

3. Jalankan perintah build_runner dengan cara menjalankan perintah berikut pada terminal:
    > flutter packages pub run build_runner build --delete-conflicting-outputs

4. Jalankan aplikasi melalui Android Studio atau VSCode