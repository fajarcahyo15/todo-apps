import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

/// Helper untuk membantu melakukan validasi pada parameter [validator] di TextFormField.
@injectable
class FormValidateHelper {

  /// Fungsi untuk mengecek apakah field diisi atau tidak (text).
  /// 
  /// Parameter [value] merupakan string data textfield untuk di cek apakah diisi atau tidak,
  /// yang kemudian akan membalikan pesan error (string) jika tidak valid.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi".
  static String textRequired({
    @required String value,
    String fieldName,
    String message,
  }) {
    int textLength = value.isNotEmpty ? value.trim().length : 0;

    if (textLength <= 0) {
      fieldName = fieldName ?? "Kolom ini";
      message = message ?? "$fieldName harus diisi";
      return message;
    } else {
      return null;
    }
  }

  /// Fungsi untuk mengecek apakah isi field kurang dari [min] karakter.
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi minimal [min] karakter".
  static String textMin({
    @required String value,
    @required int min,
    String fieldName,
    String message,
  }) {
    int textLength = value.isNotEmpty ? value.trim().length : 0;

    if (textLength < min) {
      fieldName = fieldName ?? "Kolom ini";
      message = message ?? "$fieldName harus diisi minimal $min karakter";
      return message;
    } else {
      return null;
    }
  }

  /// Fungsi untuk mengecek apakah isi field lebih dari [max] karakter.
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi maksimal [max] karakter".
  static String textMax({
    @required String value,
    @required int max,
    String fieldName,
    String message,
  }) {
    int textLength = value.isNotEmpty ? value.trim().length : 0;

    if (textLength > max) {
      fieldName = fieldName ?? "Kolom ini";
      message = message ?? "$fieldName harus diisi maksimal $max karakter";
      return message;
    } else {
      return null;
    }
  }

  /// Fungsi untuk mengecek apakah isi field diantara [min] dan [max] karakter.
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi antara [min] sampai [max] karakter".
  static String textBetween({
    @required String value,
    @required int min,
    @required int max,
    String fieldName,
    String message,
  }) {
    int textLength = (value.isNotEmpty) ? value.trim().length : 0;

    if ((textLength > max) || (textLength < min)) {
      fieldName = fieldName ?? "Kolom ini";
      message = message ?? "$fieldName harus diisi antara $min sampai $max karakter";
      return message;
    } else {
      return null;
    }
  }

  /// Fungsi untuk mengecek apakah field diisi atau tidak (number).
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi angka".
  static String numberRequired({
    @required String value,
    String fieldName,
    String message,
  }) {
    value = value.trim();
    fieldName = fieldName ?? "Kolom ini";
    message = message ?? "$fieldName harus diisi angka";

    try {
      int.parse(value);
    } catch (e) {
      return message;
    }

    return null;
  }

  /// Fungsi untuk mengecek apakah field diisi angka kurang dari [min].
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi angka minimal [min]".
  static String numberMin({
    @required String value,
    @required int min,
    String fieldName,
    String message,
  }) {
    value = value.trim();
    fieldName = fieldName ?? "Kolom ini";
    message = message ?? "$fieldName harus diisi angka minimal $min";

    try {
      int number = int.parse(value);
      if (number < min) return message;
    } catch (e) {
      return message;
    }

    return null;
  }

  /// Fungsi untuk mengecek apakah field diisi angka lebih dari [max].
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi angka maksimal [max]".
  static String numberMax({
    @required String value,
    @required int max,
    String fieldName,
    String message,
  }) {
    value = value.trim();
    fieldName = fieldName ?? "Kolom ini";
    message = message ?? "$fieldName harus diisi angka maksimal $max";

    try {
      int number = int.parse(value);
      if (number > max) return message;
    } catch (e) {
      return message;
    }

    return null;
  }

  /// Fungsi untuk mengecek apakah field diisi angka antara [min] sampai [max].
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi angka antara [min] sampai [max]".
  static String numberBetween({
    @required String value,
    @required int min,
    @required int max,
    String fieldName,
    String message,
  }) {
    value = value.trim();
    fieldName = fieldName ?? "Kolom ini";
    message = message ?? "$fieldName harus diisi angka antara $min sampai $max";

    try {
      int number = int.parse(value);
      if ((number < min) || (number > max)) return message;
    } catch (e) {
      return message;
    }

    return null;
  }

  /// Fungsi untuk mengecek apakah field diisi email.
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus diisi email".
  static String email({
    @required String value,
    String fieldName,
    String message,
  }) {
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    fieldName = fieldName ?? "Kolom ini";
    message = message ?? "$fieldName harus diisi email";

    if (!regex.hasMatch(value)) return message;

    return null;
  }

  /// Fungsi untuk mengecek apakah field diisi isinya sama dengan field lainnya.
  /// 
  /// Parameter [value] merupakan string data textfield yang menjadi sumber data pengecekan.
  /// 
  /// Parameter [textController] merupakan EditTextController yang dijadikan rujukan data untuk dibandingkan.
  /// 
  /// Parameter [matchFieldName] merupakan String berupa nama yang ingin dibandingkan (label).
  /// 
  /// Parameter [fieldName] merupakan nama kolom untuk ditampikan pada pesan error, jika tidak
  /// diisi maka akan diisi "Kolom ini".
  /// 
  /// Parameter [message] merupakan pesan yang ingin ditampilkan jika terjadi error, jika
  /// tidak diisi maka akan menampilkan "[fieldName] harus sama dengan [matchFieldName]".
  static String match({
    @required String value,
    @required TextEditingController textController,
    @required String matchFieldName,
    String fieldName,
    String message,
  }) {
    value = value.trim();
    fieldName = fieldName ?? "Kolom ini";
    message = message ?? "$fieldName harus sama dengan $matchFieldName";

    if (value != textController.text.trim()) return message;
    return null;
  }
}
