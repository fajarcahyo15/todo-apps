import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app_bba/config/modules/register.dart';
import 'package:todo_app_bba/config/theme.dart';
import 'package:todo_app_bba/controllers/friend_controller.dart';
import 'package:todo_app_bba/models/task.dart';
import 'package:todo_app_bba/screens/add_friends_screen.dart';
import 'package:todo_app_bba/widgets/dialog_custom.dart';
import 'package:todo_app_bba/widgets/snackbar_custom.dart';

class FriendsScreen extends StatefulWidget {
  final Task task;

  const FriendsScreen({Key key, this.task}) : super(key: key);

  @override
  _FriendsScreenState createState() => _FriendsScreenState();
}

class _FriendsScreenState extends State<FriendsScreen> {
  final FriendController _friendController = Get.put(getIt.get<FriendController>());
  final _tffCari = TextEditingController();

  bool _activeSearch = false;

  Widget _titleText() {
    return (widget.task.isNullOrBlank) ? Text("Teman", style: TextStyle(color: AppColors.white)) : Text("Kirim Task", style: TextStyle(color: AppColors.white));
  }

  Widget _searchTFF() {
    return TextFormField(
      controller: _tffCari,
      style: TextStyle(color: AppColors.white),
      decoration: InputDecoration(
        hintText: "Cari",
      ),
      onChanged: (val) {
        (val.isNullOrBlank) ? _friendController.getFriends() : _friendController.getFriends(search: _tffCari.text.trim());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: (_activeSearch) ? _searchTFF() : _titleText(),
        actions: [
          (_activeSearch)
              ? IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      _activeSearch = false;
                      _tffCari.text = "";
                      _friendController.getFriends();
                    });
                  },
                )
              : IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    setState(() => _activeSearch = true);
                  },
                ),
        ],
      ),
      floatingActionButton: (widget.task.isNullOrBlank) ? FloatingActionButton(child: Icon(Icons.add), onPressed: () => Get.to(AddFriendsScreen())) : null,
      body: Container(
        padding: EdgeInsets.all(AppSizes.paddingPage),
        child: Obx(() {
          if (_friendController.friends.length <= 0)
            return Center(
                child: Text(
              "Anda belum memiliki teman",
              style: TextStyle(color: AppColors.white),
            ));

          return ListView.builder(
            itemCount: _friendController.friends.length,
            itemBuilder: (ctx, index) {
              if (_friendController.friends.length <= 0) {
                return Center(
                  child: Text("Anda belum memiliki teman"),
                );
              } else if (_friendController.onProgressSendTask.value) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return Card(
                  child: ListTile(
                    leading: CachedNetworkImage(
                      imageUrl: _friendController.friends[index].photoUrl ?? "http://via.placeholder.com/350x150",
                      placeholder: (context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                    trailing: (widget.task.isNullOrBlank)
                        ? IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () {
                              _friendController.deleteFriend(_friendController.friends[index].id);
                            },
                          )
                        : IconButton(
                            icon: Icon(Icons.add),
                            onPressed: () async {
                              bool success = await _friendController.sendTask(widget.task, _friendController.friends[index]);
                              if (success) {
                                Get.dialog(DialogCustom.inform(
                                    textContent: "Berhasil mengirim task ke ${_friendController.friends[index].name}",
                                    onConfirm: () {
                                      Get.back();
                                      Get.back();
                                    }));
                              } else {
                                SnackbarCustom.error(message: _friendController.message.value);
                              }
                            },
                          ),
                    title: Text(_friendController.friends[index].name),
                    subtitle: Text(_friendController.friends[index].email),
                  ),
                );
              }
            },
          );
        }),
      ),
    );
  }
}
