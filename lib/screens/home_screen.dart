import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:todo_app_bba/config/modules/register.dart';
import 'package:todo_app_bba/config/theme.dart';
import 'package:todo_app_bba/controllers/home_controller.dart';
import 'package:todo_app_bba/screens/form_task_screen.dart';
import 'package:todo_app_bba/screens/friends_screen.dart';
import 'package:todo_app_bba/screens/login_screen.dart';
import 'package:todo_app_bba/widgets/dff_general.dart';
import 'package:todo_app_bba/widgets/tff_general.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeController _homeController = Get.put(getIt.get<HomeController>());

  final _tffDate = TextEditingController();

  String _dffCategoryValue = "-";
  DateTime _tffDateValue;

  void _onTemanPressed() async {
    Get.to(FriendsScreen());
  }

  void _onKeluarPressed() async {
    bool success = false;
    success = await _homeController.signOut();
    if (success) Get.offAll(LoginScreen());
  }

  void _onFilter() {
    _homeController.getTasks(categoryId: _dffCategoryValue, date: _tffDateValue);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Get.to(FormTaskScreen()),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(AppSizes.paddingPage),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Expanded(
                    child: AutoSizeText(
                      "Hai,\n${_homeController.name.value}",
                      style: TextStyle(
                        color: AppColors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      minFontSize: 10,
                    ),
                  ),
                  PopupMenuButton<String>(
                    icon: Icon(Icons.more_vert, color: AppColors.white),
                    itemBuilder: (BuildContext context) {
                      return {'Teman', 'Keluar'}.map((String choice) {
                        return PopupMenuItem<String>(
                          value: choice,
                          child: Text(choice),
                        );
                      }).toList();
                    },
                    onSelected: (pressed) {
                      switch (pressed) {
                        case "Teman":
                          _onTemanPressed();
                          break;
                        case "Keluar":
                          _onKeluarPressed();
                          break;
                        default:
                      }
                    },
                  ),
                ],
              ),
              SizedBox(height: AppSizes.heightBetweenDefault),
              Card(
                child: ExpansionTile(
                  title: Text("Filter", style: TextStyle(fontWeight: FontWeight.bold)),
                  children: [
                    Container(
                      padding: EdgeInsets.all(AppSizes.paddingDefault),
                      child: Column(
                        children: [
                          TFFGeneral(
                            controller: _tffDate,
                            labelText: "Tanggal",
                            prefixIcon: Icon(Icons.calendar_today_outlined),
                            onTap: () async {
                              FocusScope.of(context).requestFocus(new FocusNode());
                              final DateTime date = await showDatePicker(
                                context: context,
                                initialDate: _tffDateValue ?? DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate: DateTime.now().add(Duration(days: 7)),
                              );

                              setState(() {
                                _tffDate.text = (date.isNullOrBlank) ? "" : "${DateFormat('dd MMMM yyyy').format(date)}";
                                _tffDateValue = date;
                              });
                              _onFilter();
                            },
                          ),
                          SizedBox(height: AppSizes.heightBetweenTFF),
                          Obx(
                            () => DFFGeneral(
                              labelText: "Kategori",
                              value: _dffCategoryValue,
                              items: _homeController.categories
                                  .map(
                                    (category) => DropdownMenuItem<String>(
                                      child: Text(category.name),
                                      value: category.id,
                                    ),
                                  )
                                  .toList(),
                              onChanged: (value) => setState(() {
                                _dffCategoryValue = value;
                                _onFilter();
                              }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: AppSizes.heightBetweenDefault),
              AutoSizeText(
                "Daftar Task",
                style: TextStyle(
                  color: AppColors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
                minFontSize: 8,
              ),
              Expanded(child: Obx(() {
                if (_homeController.taskMaps.length <= 0)
                  return Center(
                    child: Text(
                      "Tidak ada task",
                      style: TextStyle(color: AppColors.white),
                    ),
                  );

                return ListView.separated(
                  shrinkWrap: true,
                  itemCount: _homeController.taskMaps.length,
                  separatorBuilder: (ctx, index) => SizedBox(height: 2),
                  itemBuilder: (ctx, index) {
                    return Card(
                      color: _homeController.taskMaps[index]['warning'] ? AppColors.yellow : null,
                      child: ListTile(
                        title: Text(
                          "${_homeController.taskMaps[index]['task'].name} | ${_homeController.taskMaps[index]['category'].name}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            AutoSizeText("${DateFormat('dd MMMM yyyy (kk:mm)').format(_homeController.taskMaps[index]['task'].date)}"),
                            SizedBox(height: 2),
                            AutoSizeText(
                              "${_homeController.taskMaps[index]['owners']}",
                              style: TextStyle(fontSize: 8),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                        // subtitle: AutoSizeText(
                        //   "${DateFormat('dd MMMM yyyy (kk:mm)').format(_homeController.taskMaps[index]['task'].date)}\n${_homeController.taskMaps[index]['owners']}",
                        //   maxLines: 2,
                        //   minFontSize: 10,
                        // ),
                        onTap: () => Get.to(FormTaskScreen(
                          task: _homeController.taskMaps[index]['task'],
                        )),
                        trailing: IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () => _homeController.deleteTask(_homeController.taskMaps[index]['task']),
                        ),
                      ),
                    );
                  },
                );
              })),
            ],
          ),
        ),
      ),
    );
  }
}
