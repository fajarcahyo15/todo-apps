import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app_bba/config/modules/register.dart';
import 'package:todo_app_bba/config/theme.dart';
import 'package:todo_app_bba/controllers/add_friend_controller.dart';
import 'package:todo_app_bba/helpers/form_validate_helper.dart';
import 'package:todo_app_bba/widgets/snackbar_custom.dart';
import 'package:todo_app_bba/widgets/tff_general.dart';

class AddFriendsScreen extends StatefulWidget {
  @override
  _AddFriendsScreenState createState() => _AddFriendsScreenState();
}

class _AddFriendsScreenState extends State<AddFriendsScreen> {
  final _keyFormSearch = GlobalKey<FormState>();
  final _addFriendController = Get.put(getIt.get<AddFriendController>());

  final _tffCari = TextEditingController();

  void _onCariPressed() async {
    bool success = await _addFriendController.getUserByEmail(_tffCari.text.toString().trim());
    if (!success) SnackbarCustom.error(message: _addFriendController.message.value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Teman", style: TextStyle(color: AppColors.white)),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: () {}),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(AppSizes.paddingPage),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Card(
                child: Container(
                  padding: EdgeInsets.all(AppSizes.paddingDefault),
                  child: Form(
                    key: _keyFormSearch,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        AutoSizeText(
                          "Tambah User Berdasarkan Email",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          minFontSize: 9,
                        ),
                        SizedBox(height: 24),
                        TFFGeneral(
                          controller: _tffCari,
                          labelText: "Email",
                          validator: (value) => FormValidateHelper.email(
                            value: value,
                            fieldName: "Email",
                          ),
                        ),
                        SizedBox(height: AppSizes.heightBetweenDefault),
                        RaisedButton(
                          color: AppColors.blue,
                          textColor: AppColors.white,
                          child: Text("Cari"),
                          onPressed: _onCariPressed,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: AppSizes.heightBetweenDefault),
              Obx(() {
                if (_addFriendController.resultSearch.value.id.isNullOrBlank) return Container();

                return Card(
                  child: ListTile(
                    leading: CachedNetworkImage(
                      imageUrl: _addFriendController.resultSearch.value.photoUrl ?? "http://via.placeholder.com/350x150",
                      placeholder: (context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () async {
                        bool success = await _addFriendController.addFriend(_addFriendController.resultSearch.value);
                        if (success) Get.back();
                      },
                    ),
                    title: Text(_addFriendController.resultSearch.value.name),
                    subtitle: Text(_addFriendController.resultSearch.value.email),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
