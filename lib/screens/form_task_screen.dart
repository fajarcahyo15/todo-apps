import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:todo_app_bba/config/modules/register.dart';
import 'package:todo_app_bba/config/theme.dart';
import 'package:todo_app_bba/controllers/task_controller.dart';
import 'package:todo_app_bba/helpers/form_validate_helper.dart';
import 'package:todo_app_bba/models/task.dart';
import 'package:todo_app_bba/screens/friends_screen.dart';
import 'package:todo_app_bba/widgets/dff_general.dart';
import 'package:todo_app_bba/widgets/tff_general.dart';

class FormTaskScreen extends StatefulWidget {
  final Task task;
  const FormTaskScreen({Key key, this.task}) : super(key: key);

  @override
  _FormTaskScreenState createState() => _FormTaskScreenState();
}

class _FormTaskScreenState extends State<FormTaskScreen> {
  final _taskController = Get.put(getIt.get<TaskController>());
  final _keyFormTask = GlobalKey<FormState>();
  final _tffDate = TextEditingController();
  final _tffName = TextEditingController();
  final _tffNote = TextEditingController();

  AutovalidateMode _formTaskAutoValidate = AutovalidateMode.disabled;
  String _dffCategoryValue;
  DateTime _tffDateValue;
  bool _isNotificate = true;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _taskController.getCategories();

      if (!widget.task.isNullOrBlank) {
        setState(() {
          _tffName.text = widget.task.name;
          _tffNote.text = widget.task.note;
          _tffDate.text = DateFormat('dd MMMM yyyy (kk:mm)').format(widget.task.date);

          _dffCategoryValue = widget.task.categoryId;
          _tffDateValue = widget.task.date;
          _isNotificate = _taskController.getNotifStatus(widget.task);
        });
      }
    });
    super.initState();
  }

  void _onSubmitPressed() async {
    if (_keyFormTask.currentState.validate()) {
      bool success = await _taskController.createTask(
        Task(
          id: (widget.task.isNullOrBlank) ? "${(DateTime.now().millisecondsSinceEpoch / 1000).floor()}" : widget.task.id,
          name: _tffName.text.toString(),
          note: _tffNote.text.toString(),
          date: _tffDateValue,
          categoryId: _dffCategoryValue,
        ),
        notif: _isNotificate,
      );
      print(success);
      if (success) Get.back();
    }
    setState(() => _formTaskAutoValidate = AutovalidateMode.always);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${(widget.task.isNullOrBlank) ? 'Buat Task Baru' : 'Edit Task'}", style: TextStyle(color: AppColors.white)),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(AppSizes.paddingPage),
          child: Column(
            children: [
              Card(
                child: SwitchListTile(
                  title: Text("Notifikasi"),
                  subtitle: Text("Set aktif untuk menampilkan notifikasi ketika task dimulai"),
                  value: _isNotificate,
                  onChanged: (val) => setState(() => _isNotificate = val),
                ),
              ),
              Card(
                child: Container(
                  padding: EdgeInsets.all(AppSizes.paddingDefault),
                  child: Form(
                    key: _keyFormTask,
                    autovalidateMode: _formTaskAutoValidate,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "Form Task",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(height: 24),
                        TFFGeneral(
                          controller: _tffDate,
                          labelText: "Tanggal",
                          prefixIcon: Icon(Icons.calendar_today_outlined),
                          onTap: () async {
                            FocusScope.of(context).requestFocus(new FocusNode());
                            final DateTime date = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(),
                              lastDate: DateTime.now().add(Duration(days: 30)),
                            );

                            if (!date.isNullOrBlank) {
                              final TimeOfDay time = await showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                              );
                              setState(() {
                                DateTime d = DateTime(date.year, date.month, date.day, time.hour, time.minute);
                                _tffDate.text = "${DateFormat('dd MMMM yyyy (kk:mm)').format(d)}";
                                _tffDateValue = d;
                              });
                            }
                          },
                          validator: (value) => FormValidateHelper.textRequired(
                            value: value,
                            fieldName: "Tanggal",
                          ),
                        ),
                        SizedBox(height: AppSizes.heightBetweenTFF),
                        Obx(
                          () => DFFGeneral(
                            labelText: "Kategori",
                            value: _dffCategoryValue,
                            items: _taskController.categories
                                .map(
                                  (category) => DropdownMenuItem<String>(
                                    child: Text(category.name),
                                    value: category.id,
                                  ),
                                )
                                .toList(),
                            onChanged: (value) => setState(() => _dffCategoryValue = value),
                            validator: (value) => (value == null) ? "Kategori harus dipilih" : null,
                          ),
                        ),
                        SizedBox(height: AppSizes.heightBetweenTFF),
                        TFFGeneral(
                          controller: _tffName,
                          labelText: "Nama",
                          validator: (value) => FormValidateHelper.textRequired(
                            value: value,
                            fieldName: "Nama",
                          ),
                        ),
                        SizedBox(height: AppSizes.heightBetweenTFF),
                        TFFGeneral(
                          controller: _tffNote,
                          labelText: "Detail",
                          maxLines: 4,
                        ),
                        SizedBox(height: AppSizes.heightBetweenTFF),
                        (!widget.task.isNullOrBlank)
                            ? RaisedButton(
                                color: AppColors.blue,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(AppSizes.radiusDefault),
                                ),
                                child: Text(
                                  "Kirim ke teman",
                                  style: TextStyle(color: AppColors.white),
                                ),
                                onPressed: () => Get.to(FriendsScreen(task: widget.task)),
                              )
                            : Container(),
                        RaisedButton(
                          color: AppColors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(AppSizes.radiusDefault),
                          ),
                          child: Text(
                            "Simpan",
                            style: TextStyle(color: AppColors.white),
                          ),
                          onPressed: _onSubmitPressed,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
