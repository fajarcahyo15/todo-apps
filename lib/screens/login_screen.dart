import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app_bba/config/modules/register.dart';
import 'package:todo_app_bba/config/theme.dart';
import 'package:todo_app_bba/controllers/login_controller.dart';
import 'package:todo_app_bba/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _loginController = Get.put(getIt.get<LoginController>());

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      bool hasLoggedIn = _loginController.checkSignIn();
      if (hasLoggedIn) Get.offAll(HomeScreen());
    });
    super.initState();
  }

  void _onLoginPressed() async {
    bool success = await _loginController.googleSignIn();
    if (success) Get.offAll(HomeScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              AutoSizeText(
                "Todo Apps",
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),
                minFontSize: 12,
                maxLines: 1,
              ),
              AutoSizeText(
                "Login",
                style: TextStyle(fontSize: 16, color: AppColors.white),
                minFontSize: 8,
                maxLines: 1,
              ),
              SizedBox(height: AppSizes.heightBetweenDefault),
              Card(
                child: Container(
                  width: Get.width * 0.7,
                  padding: EdgeInsets.all(AppSizes.paddingDefault),
                  child: RaisedButton(
                    color: Colors.red[200],
                    child: Text("Login With Google"),
                    onPressed: _onLoginPressed,
                  ),
                ),
              ),
              SizedBox(height: AppSizes.heightBetweenDefault),
              AutoSizeText(
                "Dibuat oleh: Fajar Cahyo Prabowo",
                style: TextStyle(fontSize: 12, color: AppColors.white),
                minFontSize: 8,
                maxLines: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
