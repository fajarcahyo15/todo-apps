import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_it/get_it.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/config/modules/register.config.dart';
import 'package:todo_app_bba/libraries/firebase_lib.dart';

final getIt = GetIt.instance;

@injectableInit
void configureDepedencies() => $initGetIt(getIt);

@module
abstract class RegisterModule {
  @singleton
  GoogleSignIn get googleSignIn => GoogleSignIn(scopes: [
        'email',
        'https://www.googleapis.com/auth/contacts.readonly',
      ]);

  @singleton
  GetStorage get storage => GetStorage();

  @lazySingleton
  FlutterLocalNotificationsPlugin get localNotifPlugin => FlutterLocalNotificationsPlugin();

  @singleton
  FirebaseMessaging get firebaseMessaging => FirebaseLib.config();
}
