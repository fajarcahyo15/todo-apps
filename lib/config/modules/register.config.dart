// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../../controllers/add_friend_controller.dart';
import '../../repos/category_repo.dart';
import '../../helpers/form_validate_helper.dart';
import '../../controllers/friend_controller.dart';
import '../../repos/friend_repo.dart';
import '../../controllers/home_controller.dart';
import '../../controllers/login_controller.dart';
import '../../repos/messaging_repo.dart';
import 'register.dart';
import '../../repos/session.dart';
import '../../controllers/task_controller.dart';
import '../../repos/task_repo.dart';
import '../../repos/user_repo.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final registerModule = _$RegisterModule();
  gh.factory<CategoryRepo>(() => CategoryRepo());
  gh.lazySingleton<FlutterLocalNotificationsPlugin>(
      () => registerModule.localNotifPlugin);
  gh.factory<FormValidateHelper>(() => FormValidateHelper());
  gh.factory<FriendRepo>(() => FriendRepo());
  gh.factory<MessagingRepo>(() => MessagingRepo());
  gh.factory<Session>(() => Session(get<GetStorage>()));
  gh.factory<TaskRepo>(() => TaskRepo());
  gh.factory<UserRepo>(() => UserRepo());
  gh.factory<AddFriendController>(() => AddFriendController(
        get<Session>(),
        get<UserRepo>(),
        get<FriendRepo>(),
      ));
  gh.factory<FriendController>(() => FriendController(
        get<TaskRepo>(),
        get<FriendRepo>(),
        get<Session>(),
        get<MessagingRepo>(),
      ));
  gh.factory<HomeController>(() => HomeController(
        get<GetStorage>(),
        get<CategoryRepo>(),
        get<TaskRepo>(),
        get<GoogleSignIn>(),
        get<Session>(),
        get<UserRepo>(),
        get<FirebaseMessaging>(),
        get<FlutterLocalNotificationsPlugin>(),
      ));
  gh.factory<LoginController>(() => LoginController(
        get<GoogleSignIn>(),
        get<Session>(),
        get<UserRepo>(),
        get<GetStorage>(),
        get<FirebaseMessaging>(),
      ));
  gh.factory<TaskController>(() => TaskController(
        get<Session>(),
        get<CategoryRepo>(),
        get<TaskRepo>(),
        get<FlutterLocalNotificationsPlugin>(),
        get<GetStorage>(),
      ));

  // Eager singletons must be registered in the right order
  gh.singleton<FirebaseMessaging>(registerModule.firebaseMessaging);
  gh.singleton<GetStorage>(registerModule.storage);
  gh.singleton<GoogleSignIn>(registerModule.googleSignIn);
  return get;
}

class _$RegisterModule extends RegisterModule {}
