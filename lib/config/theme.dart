import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TodoAppTheme {
  static ThemeData of(context) {
    ThemeData theme = Theme.of(context);
    return theme.copyWith(
      visualDensity: VisualDensity.adaptivePlatformDensity,
      scaffoldBackgroundColor: AppColors.blue,
      primaryColor: AppColors.black,
      textTheme: GoogleFonts.poppinsTextTheme(theme.textTheme),
      appBarTheme: AppBarTheme(
        elevation: 0,
        color: AppColors.blue,
        textTheme: GoogleFonts.poppinsTextTheme(theme.textTheme),
        centerTitle: true,
      ),
      cardTheme: CardTheme(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(AppSizes.radiusDefault)),
        color: AppColors.white,
      ),
      buttonTheme: ButtonThemeData(
        buttonColor: AppColors.black,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(AppSizes.radiusDefault)),
      ),
    );
  }
}

class AppColors {
  static const Color black = Color(0xFF212121);
  static const Color blue = Color(0xFF2390FC);
  static const Color yellow = Color(0xFFF6F797);
  static const Color white = Color(0xFFFAFAFA);
  static const Color red = Color(0xFFF23A3A);
}

class AppSizes {
  static const double paddingPage = 8;
  static const double paddingDefault = 8;
  static const double heightBetweenTFF = 16;
  static const double heightBetweenDefault = 8;
  static const double radiusDefault = 8;
}
