import 'dart:convert';

class Task {
  Task({
    this.id,
    this.name,
    this.note,
    this.date,
    this.categoryId,
    this.owners,
  });

  String id;
  String name;
  String note;
  DateTime date;
  String categoryId;
  List<String> owners;

  Task copyWith({
    String id,
    String name,
    String note,
    DateTime date,
    String categoryId,
    List<String> owners,
  }) =>
      Task(
        id: id ?? this.id,
        name: name ?? this.name,
        note: note ?? this.note,
        date: date ?? this.date,
        categoryId: categoryId ?? this.categoryId,
        owners: owners ?? this.owners,
      );

  factory Task.fromJson(String str) => Task.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Task.fromMap(Map<String, dynamic> json) => Task(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        note: json["note"] == null ? null : json["note"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        categoryId: json["category_id"] == null ? null : json["category_id"],
        owners: json["owners"] == null ? null : List<String>.from(json["owners"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "note": note == null ? null : note,
        "date": date == null ? null : date.toIso8601String(),
        "category_id": categoryId == null ? null : categoryId,
        "owners": owners == null ? null : List<dynamic>.from(owners.map((x) => x)),
      };
}
