import 'dart:convert';

class User {
  User({
    this.id,
    this.email,
    this.name,
    this.photoUrl,
    this.friends,
  });

  final String id;
  final String email;
  final String name;
  final String photoUrl;
  final List<String> friends;

  User copyWith({
    String id,
    String email,
    String name,
    String photoUrl,
    List<String> friends,
  }) =>
      User(
        id: id ?? this.id,
        email: email ?? this.email,
        name: name ?? this.name,
        photoUrl: photoUrl ?? this.photoUrl,
        friends: friends ?? this.friends,
      );

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        email: json["email"] == null ? null : json["email"],
        name: json["name"] == null ? null : json["name"],
        photoUrl: json["photoUrl"] == null ? null : json["photoUrl"],
        friends: json["friends"] == null ? null : List<String>.from(json["friends"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "email": email == null ? null : email,
        "name": name == null ? null : name,
        "photoUrl": photoUrl == null ? null : photoUrl,
        "friends": friends == null ? null : List<dynamic>.from(friends.map((x) => x)),
      };
}
