import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app_bba/config/theme.dart';

class SnackbarCustom {

  static void error({
    @required String message,
  }) {
    return Get.snackbar(
      "Terjadi Kesalahan",
      message,
      backgroundColor: Colors.red,
      snackPosition: SnackPosition.BOTTOM,
      borderRadius: AppSizes.radiusDefault,
      icon: Icon(Icons.error_rounded),
      duration: Duration(seconds: 3),
    );
  }

  static void warning({
    @required String message,
  }) {
    return Get.snackbar(
      "Perhatian",
      message,
      backgroundColor: Colors.yellow,
      snackPosition: SnackPosition.BOTTOM,
      borderRadius: AppSizes.radiusDefault,
      icon: Icon(Icons.warning_rounded),
      duration: Duration(seconds: 3),
    );
  }

  static void info({
    @required String message,
  }) {
    return Get.snackbar(
      "Informasi",
      message,
      backgroundColor: Colors.blue,
      snackPosition: SnackPosition.BOTTOM,
      borderRadius: AppSizes.radiusDefault,
      icon: Icon(Icons.info_rounded),
      duration: Duration(seconds: 3),
    );
  }
}
