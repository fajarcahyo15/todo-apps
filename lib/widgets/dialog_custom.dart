import 'package:flutter/material.dart';
import 'package:todo_app_bba/config/theme.dart';

class DialogCustom {
  static Widget inform({
    String textContent,
    VoidCallback onConfirm,
    VoidCallback onCancel,
  }) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(AppSizes.radiusDefault),
      ),
      title: Text("Informasi"),
      content: Text(textContent),
      actions: [
        FlatButton(
          child: Text("Ya"),
          onPressed: onConfirm,
        ),
      ],
    );
  }

  static Widget confirm({
    String textContent,
    VoidCallback onConfirm,
    VoidCallback onCancel,
  }) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(AppSizes.radiusDefault),
      ),
      title: Text("Konfirmasi"),
      content: Text(textContent),
      actions: [
        FlatButton(
          child: Text("Ya"),
          onPressed: onConfirm,
        ),
        FlatButton(
          child: Text("Tidak"),
          onPressed: onCancel,
        ),
      ],
    );
  }
}
