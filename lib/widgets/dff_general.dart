import 'package:flutter/material.dart';

class DFFGeneral extends StatelessWidget {
  final String labelText;
  final String prefixText;
  final Icon prefixIcon;
  final List<DropdownMenuItem<String>> items;
  final ValueChanged<String> onChanged;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final String value;

  const DFFGeneral({
    Key key,
    @required this.labelText,
    this.prefixText,
    this.prefixIcon,
    @required this.items,
    this.onChanged,
    this.onSaved,
    this.validator,
    this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      key: key,
      decoration: InputDecoration(
        labelText: labelText,
        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
        prefixText: prefixText,
        prefixIcon: prefixIcon,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      value: value,
      items: items,
      onChanged: onChanged,
      onSaved: onSaved,
      validator: validator,
    );
  }
}
