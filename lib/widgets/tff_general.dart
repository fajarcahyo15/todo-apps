import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

enum TFFGeneralFor {
  currency,
  phone,
  normal,
}

class TFFGeneral extends StatelessWidget {
  final TextEditingController controller;
  final TFFGeneralFor usedFor;
  final String labelText;
  final TextInputType keyboardType;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final bool obscureText;
  final int maxLines;
  final bool enabled;
  final String prefixText;
  final Icon prefixIcon;
  final TextAlign textAlign;
  final ValueChanged<String> onChanged;
  final GestureTapCallback onTap;

  const TFFGeneral({
    Key key,
    @required this.labelText,
    this.keyboardType = TextInputType.text,
    this.usedFor = TFFGeneralFor.normal,
    this.validator,
    this.onSaved,
    this.obscureText = false,
    this.maxLines = 1,
    this.controller,
    this.enabled = true,
    this.prefixText = "",
    this.prefixIcon,
    this.textAlign = TextAlign.start,
    this.onChanged,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: key,
      controller: controller,
      enabled: enabled,
      decoration: InputDecoration(
        labelText: labelText,
        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
        prefixText: prefixText,
        prefixIcon: prefixIcon,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      keyboardType: keyboardType,
      validator: validator,
      onSaved: onSaved,
      obscureText: obscureText,
      maxLines: maxLines,
      textAlign: textAlign,
      onChanged: onChanged,
      onTap: onTap,
    );
  }
}
