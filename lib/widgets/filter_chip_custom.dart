import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:todo_app_bba/config/theme.dart';

class FilterChipCustom extends StatelessWidget {
  final String label;
  final bool selected;
  final ValueChanged<bool> onSelected;

  const FilterChipCustom({
    Key key,
    @required this.label,
    this.selected = false,
    this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FilterChip(
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(AppSizes.radiusDefault)),
      selectedColor: AppColors.white,
      backgroundColor: AppColors.white,
      selected: selected,
      label: Text(
        label,
        style: GoogleFonts.poppins(
          color: AppColors.black,
          fontWeight: (selected) ? FontWeight.bold : null,
        ),
      ),
      onSelected: onSelected,
    );
  }
}
