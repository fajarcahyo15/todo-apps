import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

@injectable
class CategoryRepo {
  final CollectionReference _categoryCollection = FirebaseFirestore.instance.collection('categories');

  Future<QuerySnapshot> getCategories() async {
    return await _categoryCollection.get();
  }

  Future<QuerySnapshot> getById(String categoryId) async {
    return await _categoryCollection.where("id", isEqualTo: categoryId).get();
  }

  Stream<QuerySnapshot> getStreamCategories() {
    return _categoryCollection.snapshots();
  }
}