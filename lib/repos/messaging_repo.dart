import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:http/http.dart' as http;
import 'package:todo_app_bba/config/strings.dart';

@injectable
class MessagingRepo {
  Future sendNotif({
    @required String userId,
    @required String title,
    @required String body,
  }) async {
    try {
      final res = await http.post(
        "https://fcm.googleapis.com/fcm/send",
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "key=${Strings.fToken}",
        },
        body: jsonEncode({
          "to": "/topics/$userId",
          "notification": {"body": body, "title": title},
          "priority": "high",
          "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "id": "1", "status": "done"}
        }),
      );

      print("Send Notif: ${res.statusCode}");
    } on SocketException {
      print("Error MessagingRepo@sendNotif: SocketException");
    } catch (e) {
      print("Error MessagingRepo@sendNotif: $e");
    }
  }
}
