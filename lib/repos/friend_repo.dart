import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/models/user.dart';

@injectable
class FriendRepo {
  final CollectionReference _collection = FirebaseFirestore.instance.collection('users');

  Stream<QuerySnapshot> getFriends(String userId, {String search}) {
    Query query = _collection.doc(userId).collection('friends').where('name', isEqualTo: search);
    return query.snapshots();
  }

  Future<void> createOrUpdate(User user, User friend) async {
    await _collection.doc(user.id).collection('friends').doc(friend.id).set(friend.toMap());
    await _collection.doc(friend.id).collection('friends').doc(user.id).set(user.toMap());
  }

  Future<void> delete(String userId, String friendId) async {
    await _collection.doc(userId).collection('friends').doc(friendId).delete();
    await _collection.doc(friendId).collection('friends').doc(userId).delete();
  }
}