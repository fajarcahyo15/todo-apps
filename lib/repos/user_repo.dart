import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/models/user.dart';

@injectable
class UserRepo {
  final CollectionReference _userCollection = FirebaseFirestore.instance.collection('users');

  Stream<QuerySnapshot> getUsers(String userId, {String name}) {
    Query query = _userCollection.where('id', isNotEqualTo: userId);
    if ((name != null) && (name != "")) query.where('name', isEqualTo: name);
    return query.snapshots();
  }

  Future<QuerySnapshot> getUserByEmail(String userId, String email) {
    return _userCollection.where('email', isEqualTo: email).get();
  }

  Future<QuerySnapshot> getById(String userId) async {
    return await _userCollection.where("id", isEqualTo: userId).get();
  }

  Future<QuerySnapshot> searchUser(String name) async {
    return await _userCollection.where('name', isEqualTo: name).get();
  }

  Future<void> createOrUpdateUser(User user) async {
    await _userCollection.doc(user.id).set(user.toMap());
  }

  Future<void> createOrUpdateFriend(User user, User friend) async {
    await _userCollection.doc(user.id).collection('friends').doc(friend.id).set(friend.toMap());
    await _userCollection.doc(friend.id).collection('friends').doc(user.id).set(user.toMap());
  }

  Future<void> deleteFriend(User user, User friend) async {
    await _userCollection.doc(user.id).collection('friends').doc(friend.id).delete();
    await _userCollection.doc(friend.id).collection('friends').doc(user.id).delete();
  }
}
