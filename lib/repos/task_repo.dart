import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/models/task.dart';

@injectable
class TaskRepo {
  final CollectionReference _taskCollection = FirebaseFirestore.instance.collection('tasks');

  Future createOrUpdate(Task task) async {
    await _taskCollection.doc(task.id).set(task.toMap());
  }

  Future deleteTask(String taskId) async {
    await _taskCollection.doc(taskId).delete();
  }

  Stream<QuerySnapshot> getStreamTask(String userId, {String categoryId, DateTime date}) {
    date = date ?? DateTime.now();
    return _taskCollection.where('owners', arrayContains: userId).where('category_id', isEqualTo: categoryId).snapshots();
  }
}
