import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/config/strings.dart';
import 'package:todo_app_bba/models/user.dart';

@injectable
class Session {
  final GetStorage box;

  Session(this.box);

  void writeAccount({@required String name, @required String email, @required String id, String photoUrl}) {
    box.write(Strings.boxId, id);
    box.write(Strings.boxName, name);
    box.write(Strings.boxEmail, email);
    box.write(Strings.boxPhoto, photoUrl);
  }

  void removeAccount() {
    box.remove(Strings.boxId);
    box.remove(Strings.boxName);
    box.remove(Strings.boxEmail);
    box.remove(Strings.boxPhoto);
  }

  User getUser() {
    return User(
      id: box.read(Strings.boxId),
      email: box.read(Strings.boxEmail),
      name: box.read(Strings.boxName),
      photoUrl: box.read(Strings.boxPhoto),
    );
  }
}
