import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotifLib {
  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  static init() async {
    final initializeSettingsAndroid = AndroidInitializationSettings('app_icon');
    final initializationSettingsIOS = IOSInitializationSettings();
    final initializationSettings = InitializationSettings(android: initializeSettingsAndroid, iOS: initializationSettingsIOS);

    await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: (payload) async {
      print("Notification Payload: $payload");
    });

    return flutterLocalNotificationsPlugin;
  }

  static NotificationDetails channel() {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'SEND TASK',
      'SEND TASK',
      'SEND TASK',
      importance: Importance.max,
      priority: Priority.high,
      ticker: "ticker",
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );

    return platformChannelSpecifics;
  }
}
