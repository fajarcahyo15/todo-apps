import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:todo_app_bba/libraries/notif_lib.dart';

class FirebaseLib {
  static FlutterLocalNotificationsPlugin _notification = FlutterLocalNotificationsPlugin();

  static FirebaseMessaging config() {
    FirebaseMessaging firebaseMessaging = FirebaseMessaging();

    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _notification.show(10, message['notification']['title'], message['notification']['body'], NotifLib.channel());
        
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        _notification.show(10, message['notification']['title'], message['notification']['body'], NotifLib.channel());
        
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        _notification.show(10, message['notification']['title'], message['notification']['body'], NotifLib.channel());
        
      },
      onBackgroundMessage: onBackground,
    );

    firebaseMessaging.requestNotificationPermissions(const IosNotificationSettings(
      sound: true,
      badge: true,
      alert: true,
      provisional: true,
    ));
    firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    return firebaseMessaging;
  }

  static Future<dynamic> onBackground(Map<String, dynamic> message) async {
    print("onBackground: $message");
    _notification.show(10, message['notification']['title'], message['notification']['body'], NotifLib.channel());
  }
}
