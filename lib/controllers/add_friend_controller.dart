import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/models/user.dart';
import 'package:todo_app_bba/repos/friend_repo.dart';
import 'package:todo_app_bba/repos/session.dart';
import 'package:todo_app_bba/repos/user_repo.dart';

@injectable
class AddFriendController extends GetxController {
  final Session _session;
  final FriendRepo _friendRepo;
  final UserRepo _userRepo;

  AddFriendController(this._session, this._userRepo, this._friendRepo);

  Rx<User> resultSearch = User().obs;
  RxString message = "".obs;

  User activeUser;

  @override
  void onInit() {
    activeUser = _session.getUser();
    super.onInit();
  }

  Future<bool> getUserByEmail(String email) async {
    bool success = false;

    try {
      resultSearch.value = User();
      QuerySnapshot userSnapshot = await _userRepo.getUserByEmail(activeUser.id, email);
      if (activeUser.id != userSnapshot.docs[0].data()['id']) {
        resultSearch.value = User(
          id: userSnapshot.docs[0].data()['id'],
          email: userSnapshot.docs[0].data()['email'],
          name: userSnapshot.docs[0].data()['name'],
          photoUrl: userSnapshot.docs[0].data()['photoUrl'],
        );
        success = true;
      } else {
        message.value = "User tidak ditemukan";
      }
    } catch (e) {
      message.value = "User tidak ditemukan";
      print("Error AddFriendController@getUserByEmail: $e");
    }

    return success;
  }

  Future<bool> addFriend(User friend) async {
    bool success = false;

    try {
      await _friendRepo.createOrUpdate(activeUser, friend);
      success = true;
    } catch (e) {
      print("Error AddFriendController@addFriend: $e");
    }

    return success;
  }
}
