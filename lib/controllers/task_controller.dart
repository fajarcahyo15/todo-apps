import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/config/strings.dart';
import 'package:todo_app_bba/libraries/notif_lib.dart';
import 'package:todo_app_bba/models/category.dart';
import 'package:todo_app_bba/models/task.dart';
import 'package:todo_app_bba/repos/category_repo.dart';
import 'package:todo_app_bba/repos/session.dart';
import 'package:todo_app_bba/repos/task_repo.dart';

@injectable
class TaskController extends GetxController {
  final Session _session;
  final CategoryRepo _categoryRepo;
  final TaskRepo _taskRepo;
  final FlutterLocalNotificationsPlugin _notification;
  final GetStorage _box;

  TaskController(this._session, this._categoryRepo, this._taskRepo, this._notification, this._box);

  RxList<Category> categories = List<Category>().obs;

  Future getCategories() async {
    try {
      QuerySnapshot snapshot = await _categoryRepo.getCategories();
      List<Category> categories = snapshot.docs.map((category) {
        return Category(
          id: category.data()["id"],
          name: category.data()["name"],
        );
      }).toList();

      this.categories.assignAll(categories);
    } catch (e) {
      print("Error TaskController@getCategories: $e");
    }
  }

  bool getNotifStatus(Task task) {
    bool notif = false;
    if (_box.hasData(task.id)) {
      notif = _box.read(task.id);
    }
    return notif;
  }

  Future<bool> createTask(Task task, {bool notif = true}) async {
    bool success = false;
    List<String> userIds = List<String>();
    userIds.add(_session.box.read(Strings.boxId));
    task.owners = task.owners ?? userIds;

    try {
      await _taskRepo.createOrUpdate(task);
      await _notification.cancel(int.parse(task.id));
      _box.write(task.id, false);
      if (notif) {
        await _notification.schedule(
          int.parse(task.id),
          task.name,
          task.note ?? "-",
          task.date,
          NotifLib.channel(),
        );
        _box.write(task.id, true);
      }

      success = true;
    } catch (e) {
      print("Error TaskController@createTask: $e");
    }

    return success;
  }
}
