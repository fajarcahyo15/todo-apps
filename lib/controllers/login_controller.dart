import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/config/strings.dart';
import 'package:todo_app_bba/models/user.dart';
import 'package:todo_app_bba/repos/session.dart';
import 'package:todo_app_bba/repos/user_repo.dart';

@injectable
class LoginController extends GetxController {
  final GoogleSignIn _googleSignIn;
  final Session _session;
  final UserRepo _userRepo;
  final GetStorage _box;
  final FirebaseMessaging _firebaseMessaging;

  RxBool loading = false.obs;
  RxString message = "".obs;

  LoginController(this._googleSignIn, this._session, this._userRepo, this._box, this._firebaseMessaging);

  bool checkSignIn() {
    bool success = false;
    if (_box.hasData(Strings.boxId)) success = true;
    return success;
  }

  Future<bool> googleSignIn() async {
    bool success = false;
    loading.value = true;

    try {
      GoogleSignInAccount accountData = await _googleSignIn.signIn();

      _session.writeAccount(
        name: accountData.displayName,
        email: accountData.email,
        id: accountData.id,
        photoUrl: accountData.photoUrl,
      );

      await _userRepo.createOrUpdateUser(User(
        id: accountData.id,
        email: accountData.email,
        name: accountData.displayName,
        photoUrl: accountData.photoUrl,
      ));
      
      _firebaseMessaging.subscribeToTopic(accountData.id);
      success = true;
    } catch (e) {
      print("Error LoginController@googleSignIn: $e");
    }

    loading.value = false;
    return success;
  }
}
