import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/config/strings.dart';
import 'package:todo_app_bba/models/category.dart';
import 'package:todo_app_bba/models/task.dart';
import 'package:todo_app_bba/models/user.dart';
import 'package:todo_app_bba/repos/category_repo.dart';
import 'package:todo_app_bba/repos/session.dart';
import 'package:todo_app_bba/repos/task_repo.dart';
import 'package:todo_app_bba/repos/user_repo.dart';

@injectable
class HomeController extends GetxController {
  final GoogleSignIn _googleSignIn;
  final Session _session;
  final GetStorage _box;
  final CategoryRepo _categoryRepo;
  final TaskRepo _taskRepo;
  final UserRepo _userRepo;
  final FirebaseMessaging _firebaseMessaging;
  final FlutterLocalNotificationsPlugin _notification;

  RxString name = "".obs;
  RxString photoUrl = "".obs;
  RxList<Category> categories = List<Category>().obs;
  RxList<Map<String, dynamic>> taskMaps = List<Map<String, dynamic>>().obs;
  StreamSubscription<QuerySnapshot> _taskSnapshots;

  User _activeUser;

  HomeController(this._box, this._categoryRepo, this._taskRepo, this._googleSignIn, this._session, this._userRepo, this._firebaseMessaging, this._notification);

  @override
  void onInit() {
    _activeUser = _session.getUser();

    getAccount();
    getCategories();
    getTasks();
    super.onInit();
  }

  @override
  void onClose() {
    _taskSnapshots?.cancel();
    super.onClose();
  }

  Future getCategories() async {
    try {
      QuerySnapshot snapshot = await _categoryRepo.getCategories();
      List<Category> categories = snapshot.docs.map((category) {
        return Category(
          id: category.data()["id"],
          name: category.data()["name"],
        );
      }).toList();

      categories.insert(0, Category(id: "-", name: "Semua"));
      this.categories.assignAll(categories);
    } catch (e) {
      print("Error HomeController@getCategories: $e");
    }
  }

  Future<Category> getCategoryById(String categoryId) async {
    QuerySnapshot snapshot = await _categoryRepo.getById(categoryId);
    Category category = Category(
      id: snapshot.docs[0].data()['id'],
      name: snapshot.docs[0].data()['name'],
    );
    return category;
  }

  void getAccount() {
    if (_box.hasData(Strings.boxName)) {
      name.value = _box.read(Strings.boxName);
      photoUrl.value = _box.read(Strings.boxPhoto);
    }
  }

  void getTasks({String categoryId, DateTime date}) async {
    categoryId = (categoryId != "-") ? categoryId : null;
    _taskSnapshots = _taskRepo.getStreamTask(_box.read(Strings.boxId), categoryId: categoryId).listen((snapshot) async {
      try {
        List<Map<String, dynamic>> taskMaps = List<Map<String, dynamic>>();

        for (QueryDocumentSnapshot snap in snapshot.docs) {
          var diffDateDay = DateTime.parse(snap.data()['date']).difference(DateTime.now()).inDays;
          var diffDateFilter = (!date.isNullOrBlank) ? DateTime.parse(snap.data()['date']).difference(date).inDays : null;
          if ((!diffDateFilter.isNullOrBlank) && (diffDateFilter == 0)) {
            Task task = Task(
              id: snap.data()['id'],
              name: snap.data()['name'],
              note: snap.data()['note'],
              date: DateTime.parse(snap.data()['date']),
              categoryId: snap.data()['category_id'],
              owners: List<String>.from(snap.data()['owners']),
            );

            QuerySnapshot snapCategory = await _categoryRepo.getById(snap.data()['category_id']);
            Category category = Category(
              id: snapCategory.docs[0].data()['id'],
              name: snapCategory.docs[0].data()['name'],
            );

            String owners = "";
            for (String userId in task.owners) {
              QuerySnapshot snapCategory = await _userRepo.getById(userId);
              owners = "$owners, ${snapCategory.docs[0].data()['name']}";
            }
            owners = owners.substring(1);

            bool warning = (DateTime.parse(snap.data()['date']).difference(DateTime.now()).inMinutes <= 60);
            taskMaps.add({
              'task': task,
              'category': category,
              'warning': warning,
              'owners': owners,
            });
          } else if ((diffDateFilter.isNullOrBlank) && (diffDateDay >= 0)) {
            Task task = Task(
              id: snap.data()['id'],
              name: snap.data()['name'],
              note: snap.data()['note'],
              date: DateTime.parse(snap.data()['date']),
              categoryId: snap.data()['category_id'],
              owners: List<String>.from(snap.data()['owners']),
            );

            QuerySnapshot snapCategory = await _categoryRepo.getById(snap.data()['category_id']);
            Category category = Category(
              id: snapCategory.docs[0].data()['id'],
              name: snapCategory.docs[0].data()['name'],
            );

            String owners = "";
            for (String userId in task.owners) {
              QuerySnapshot snapCategory = await _userRepo.getById(userId);
              owners = "$owners, ${snapCategory.docs[0].data()['name'].split(' ')[0]}";
            }
            owners = owners.substring(1);

            bool warning = (DateTime.parse(snap.data()['date']).difference(DateTime.now()).inMinutes <= 60);
            taskMaps.add({
              'task': task,
              'category': category,
              'warning': warning,
              'owners': owners,
            });
          }
        }

        this.taskMaps.assignAll(taskMaps);
      } catch (e) {
        print("Error HomeController@getTasks: $e");
      }
    });
  }

  Future<bool> deleteTask(Task task) async {
    bool success = false;

    try {
      task.owners.removeWhere((userId) => userId == _activeUser.id);

      if (task.owners.length <= 0)
        await _taskRepo.deleteTask(task.id);
      else
        await _taskRepo.createOrUpdate(task);

      if (_box.hasData(task.id)) {
        if (_box.read(task.id)) {
          await _notification.cancel(int.parse(task.id));
        }
        _box.remove(task.id);
      }

      success = true;
    } catch (e) {
      print("Error HomeController@deleteTask: $e");
    }

    return success;
  }

  Future<bool> signOut() async {
    bool success = false;
    User user = _session.getUser();

    try {
      await _googleSignIn.signOut();
      _session.removeAccount();
      _firebaseMessaging.unsubscribeFromTopic(user.id);

      for (var item in taskMaps) {
        try {
          _box.remove(item['task'].id.toString());
        } catch (e) {
          print("Error HomeController@signOut: $e");
        }
      }

      success = true;
    } catch (e) {
      print("Error HomeController@signOut: $e");
    }

    return success;
  }
}
