import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app_bba/models/task.dart';
import 'package:todo_app_bba/models/user.dart';
import 'package:todo_app_bba/repos/friend_repo.dart';
import 'package:todo_app_bba/repos/messaging_repo.dart';
import 'package:todo_app_bba/repos/session.dart';
import 'package:todo_app_bba/repos/task_repo.dart';

@injectable
class FriendController extends GetxController {
  final Session _session;
  final TaskRepo _taskRepo;
  final FriendRepo _friendRepo;
  final MessagingRepo _messagingRepo;

  FriendController(this._taskRepo, this._friendRepo, this._session, this._messagingRepo);

  RxList<User> friends = List<User>().obs;
  RxList<User> users = List<User>().obs;
  RxString message = "".obs;
  RxBool onProgressSendTask = false.obs;
  StreamSubscription<QuerySnapshot> _friendsSnapshot;
  User _userActive;

  @override
  void onInit() {
    _userActive = _session.getUser();
    getFriends();
    super.onInit();
  }

  @override
  void onClose() {
    _friendsSnapshot?.cancel();
    super.onClose();
  }

  Future getFriends({String search}) async {
    try {
      _friendsSnapshot = _friendRepo.getFriends(_userActive.id, search: search).listen((snapshot) {
        List<User> friends = snapshot.docs.map((snap) {
          return User(
            id: snap.data()['id'],
            email: snap.data()['email'],
            name: snap.data()['name'],
            photoUrl: snap.data()['photoUrl'],
          );
        }).toList();

        this.friends.assignAll(friends);
      });
    } catch (e) {
      print("Error FriendController@getFriends: $e");
    }
  }

  Future<bool> sendTask(Task task, User user) async {
    bool success = false;
    onProgressSendTask.value = true;

    try {
      if (!task.owners.contains(user.id)) {
        task.owners.add(user.id);
        await _taskRepo.createOrUpdate(task);
        await _messagingRepo.sendNotif(
          userId: user.id,
          title: "Task Baru",
          body: "Ada task ${task.name} dari ${user.name}.",
        );
        success = true;
      } else {
        message.value = "Task sudah dikirim ke user";
      }
    } catch (e) {
      print("Error FriendController@sendTask: $e");
      message.value = "Gagal mengirim task";
    }

    onProgressSendTask.value = false;
    return success;
  }

  Future deleteFriend(String friendId) async {
    try {
      await _friendRepo.delete(_userActive.id, friendId);
    } catch (e) {
      print("Error FriendController@deleteFriend: $e");
    }
  }
}
